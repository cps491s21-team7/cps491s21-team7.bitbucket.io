University of Dayton

Department of Computer Science

CPS 490 - Spring, 2021

## Capstone II Proposal

# Topic

Efficient File Content Searching at Scale

# Team members

1. Andrew Streng, strenga1@udayton.edu
2. Roberto Valadez, valadezjrr1@udayton.edu
3. Nolan Hollingsworth, hollingsworthn2@udayton.edu

# Company Mentors

Jeff Archer-Jeffrey.Archer@ge.com

GE Aviation

Company address

## Project homepage

<https://cps491s21-team7.bitbucket.io/>
